---
"@remix-run/router": patch
---

Fix an issue in `queryRoute` that was not always identifying thrown `Response` instances
