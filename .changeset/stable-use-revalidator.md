---
"react-router": patch
---

Ensure `useRevalidator` is referentially stable across re-renders if revalidations are not actively occuring
